#!/bin/bash

u=$1
a=$(id -u $u 2> /dev/null)
while true
do
	echo -e "(a) Verificar existencia usuario\n(b) Verificar se usuario esta logado\n(c) Listar home do Usuario\n(d) Sair"
       	read -p "Digite uma opcao: " y
	case $y in
		a)grep $u /etc/passwd &> /dev/null && echo "Existe" || echo "Nao Existe";;
		b)([ $a -eq 1000 ] &> /dev/null && echo "Esta logado") || (grep $u /etc/passwd &> /dev/null && echo "Nao esta") || (echo "ERRO USUARIO NAO EXISTE");;
		c)(ls /home/$u 2> /dev/null) || (echo "ERRO USUARIO NAO EXISTE");;
		d)exit 0;;
	esac
done
